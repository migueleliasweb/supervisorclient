<?php

class XMLRPCClient
{

    private $Host;
    private $CurlResource;

    /**
     * @param string $Host
     */
    public function __construct ($Host) {
        $this->setHost($Host);
        $this->setCurlResource(curl_init());
    }
    
    public function __destruct () {
        $this->close();
    }
    
    /**
     * @return string
     */
    public function getHost () {
        return $this->Host;
    }

    /**
     * @param string $Hos
     */
    private function setHost ($Host) {
        $this->Host = $Host;
    }

    /**
     * @return \Resource
     */
    public function getCurlResource () {
        return $this->CurlResource;
    }

    private function setCurlResource ($CurlResource) {
        $this->CurlResource = $CurlResource;
    }

    public function close () {
        curl_close($this->getCurlResource());
    }
    
    public function request ($method, $params = array()) {
        $encoded_request = xmlrpc_encode_request($method, $params);

        /* Configure the request */
        curl_setopt($this->getCurlResource(), CURLOPT_URL, $this->getHost());
        curl_setopt($this->getCurlResource(), CURLOPT_HEADER, 0);
        curl_setopt($this->getCurlResource(), CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->getCurlResource(), CURLOPT_POST, true);

        curl_setopt($this->getCurlResource(), CURLOPT_POSTFIELDS, $encoded_request);

        $result = xmlrpc_decode_request(curl_exec($this->getCurlResource()), $method);

        return $result;
    }

}