<?php

require_once 'XMLRPCClient.php';

/**
    * All docs here were extracted from the API itself. Make sure the 
    * SUPPORTED_API_VERSION is the SAME as the running version.
    * This class is a wrapper for Supervidord 3.0 API.
 */
class SupervisorClient extends XMLRPCClient
{
    
    const SUPPORTED_API_VERSION = '3.0';
    
    /**
     * Update the config for a running process from config file.
     * @param string name         name of process group to add
     * @return boolean result     true if successful
     */
    public function addProcessGroup () {
        return $this->request('supervisor.addProcessGroup', func_get_args());
    }

    /**
     * Clear all process log files
     * @return boolean result      Always return true
     */
    public function clearAllProcessLogs () {
        return $this->request('supervisor.clearAllProcessLogs', func_get_args());
    }

    /**
     * Clear the main log.
     * @return boolean result always returns True unless error
     */
    public function clearLog () {
        return $this->request('supervisor.clearLog', func_get_args());
    }

    /**
     * Clear the stdout and stderr logs for the named process and
     * reopen them.
     * @param string name   The name of the process (or 'group:name')
     * @return boolean result      Always True unless error
     */
    public function clearProcessLog () {
        return $this->request('supervisor.clearProcessLog', func_get_args());
    }

    /**
     * Clear the stdout and stderr logs for the named process and
     * reopen them.
     * @param string name   The name of the process (or 'group:name')
     * @return boolean result      Always True unless error
     */
    public function clearProcessLogs () {
        return $this->request('supervisor.clearProcessLogs', func_get_args());
    }

    /**
     * Return the version of the RPC API used by supervisord
     * @return string version version id
     */
    public function getAPIVersion () {
        return $this->request('supervisor.getAPIVersion', func_get_args());
    }

    /**
     * Get info about all availible process configurations. Each record
     * represents a single process (i.e. groups get flattened).
     * @return array result  An array of process config info records
     */
    public function getAllConfigInfo () {
        return $this->request('supervisor.getAllConfigInfo', func_get_args());
    }

    /**
     * Get info about all processes
     * @return array result  An array of process status results
     */
    public function getAllProcessInfo () {
        return $this->request('supervisor.getAllProcessInfo', func_get_args());
    }

    /**
     * Return identifiying string of supervisord
     * @return string identifier identifying string
     */
    public function getIdentification () {
        return $this->request('supervisor.getIdentification', func_get_args());
    }

    /**
     * Return the PID of supervisord
     * @return int PID
     */
    public function getPID () {
        return $this->request('supervisor.getPID', func_get_args());
    }

    /**
     * Get info about a process named name
     * @param string name The name of the process (or 'group:name')
     * @return struct result     A structure containing data about the process
     */
    public function getProcessInfo () {
        return $this->request('supervisor.getProcessInfo', func_get_args());
    }

    /**
     * Return current state of supervisord as a struct
     * @return struct A struct with keys string statecode, int statename
     */
    public function getState () {
        return $this->request('supervisor.getState', func_get_args());
    }

    /**
     * Return the version of the supervisor package in use by supervisord
     * @return string version version id
     */
    public function getSupervisorVersion () {
        return $this->request('supervisor.getSupervisorVersion', func_get_args());
    }

    /**
     * Return the version of the RPC API used by supervisord
     * @return string version version id
     */
    public function getVersion () {
        return $this->request('supervisor.getVersion', func_get_args());
    }

    /**
     * Read length bytes from the main log starting at offset
     * @param int offset         offset to start reading from.
     * @param int length         number of bytes to read from the log.
     * @return string result     Bytes of log
     */
    public function readLog () {
        return $this->request('supervisor.readLog', func_get_args());
    }

    /**
     * Read length bytes from the main log starting at offset
     * @param int offset         offset to start reading from.
     * @param int length         number of bytes to read from the log.
     * @return string result     Bytes of log
     */
    public function readMainLog () {
        return $this->request('supervisor.readMainLog', func_get_args());
    }

    /**
     * Read length bytes from name's stdout log starting at offset
     * @param string name        the name of the process (or 'group:name')
     * @param int offset         offset to start reading from.
     * @param int length         number of bytes to read from the log.
     * @return string result     Bytes of log
     */
    public function readProcessLog () {
        return $this->request('supervisor.readProcessLog', func_get_args());
    }

    /**
     * Read length bytes from name's stderr log starting at offset
     * @param string name        the name of the process (or 'group:name')
     * @param int offset         offset to start reading from.
     * @param int length         number of bytes to read from the log.
     * @return string result     Bytes of log
     */
    public function readProcessStderrLog () {
        return $this->request('supervisor.readProcessStderrLog', func_get_args());
    }

    /**
     * Read length bytes from name's stdout log starting at offset
     * @param string name        the name of the process (or 'group:name')
     * @param int offset         offset to start reading from.
     * @param int length         number of bytes to read from the log.
     * @return string result     Bytes of log
     */
    public function readProcessStdoutLog () {
        return $this->request('supervisor.readProcessStdoutLog', func_get_args());
    }

    /**
     * Reload configuration
     * @return boolean result  always return True unless error
     */
    public function reloadConfig () {
        return $this->request('supervisor.reloadConfig', func_get_args());
    }

    /**
     * Remove a stopped process from the active configuration.
     * @param string name         name of process group to remove
     * @return boolean result     Indicates wether the removal was successful
     */
    public function removeProcessGroup () {
        return $this->request('supervisor.removeProcessGroup', func_get_args());
    }

    /**
     * Restart the supervisor process
     * @return boolean result  always return True unless error
     */
    public function restart () {
        return $this->request('supervisor.restart', func_get_args());
    }

    /**
     * Send a string of chars to the stdin of the process name.
     * If non-7-bit data is sent (unicode), it is encoded to utf-8
     * before being sent to the process' stdin.  If chars is not a
     * string or is not unicode, raise INCORRECT_PARAMETERS.  If the
     * process is not running, raise NOT_RUNNING.  If the process'
     * stdin cannot accept input (e.g. it was closed by the child
     * process), raise NO_FILE.
     * @param string name        The process name to send to (or 'group:name')
     * @param string chars       The character data to send to the process
     * @return boolean result    Always return True unless error
     */
    public function sendProcessStdin () {
        return $this->request('supervisor.sendProcessStdin', func_get_args());
    }

    /**
     * Send an event that will be received by event listener
     * subprocesses subscribing to the RemoteCommunicationEvent.
     * @param  string  type  String for the "type" key in the event header
     * @param  string  data  Data for the event body
     * @return boolean       Always return True unless error
     */
    public function sendRemoteCommEvent () {
        return $this->request('supervisor.sendRemoteCommEvent', func_get_args());
    }

    /**
     * Shut down the supervisor process
     * @return boolean result always returns True unless error
     */
    public function shutdown () {
        return $this->request('supervisor.shutdown', func_get_args());
    }

    /**
     * Start all processes listed in the configuration file
     * @param boolean wait Wait for each process to be fully started
     * @return struct result     A structure containing start statuses
     */
    public function startAllProcesses () {
        return $this->request('supervisor.startAllProcesses', func_get_args());
    }

    /**
     * Start a process
     * @param string name Process name (or 'group:name', or 'group:*')
     * @param boolean wait Wait for process to be fully started
     * @return boolean result     Always true unless error
     */
    public function startProcess () {
        return $this->request('supervisor.startProcess', func_get_args());
    }

    /**
     * Start all processes in the group named 'name'
     * @param string name        The group name
     * @param boolean wait       Wait for each process to be fully started
     * @return struct result     A structure containing start statuses
     */
    public function startProcessGroup () {
        return $this->request('supervisor.startProcessGroup', func_get_args());
    }

    /**
     * Stop all processes in the process list
     * @param boolean wait    Wait for each process to be fully stopped
     * @return boolean result Always return true unless error.
     */
    public function stopAllProcesses () {
        return $this->request('supervisor.stopAllProcesses', func_get_args());
    }

    /**
     * Stop a process named by name
     * @param string name  The name of the process to stop (or 'group:name')
     * @param boolean wait        Wait for the process to be fully stopped
     * @return boolean result     Always return True unless error
     */
    public function stopProcess () {
        return $this->request('supervisor.stopProcess', func_get_args());
    }

    /**
     * Stop all processes in the process group named 'name'
     * @param string name  The group name
     * @param boolean wait    Wait for each process to be fully stopped
     * @return boolean result Always return true unless error.
     */
    public function stopProcessGroup () {
        return $this->request('supervisor.stopProcessGroup', func_get_args());
    }

    /**
     * Provides a more efficient way to tail the (stdout) log than
     * readProcessStdoutLog().  Use readProcessStdoutLog() to read
     * chunks and tailProcessStdoutLog() to tail.
     * Requests (length) bytes from the (name)'s log, starting at
     * (offset).  If the total log size is greater than (offset +
     * length), the overflow flag is set and the (offset) is
     * automatically increased to position the buffer at the end of
     * the log.  If less than (length) bytes are available, the
     * maximum number of available bytes will be returned.  (offset)
     * returned is always the last offset in the log +1.
     * @param string name         the name of the process (or 'group:name')
     * @param int offset          offset to start reading from
     * @param int length          maximum number of bytes to return
     * @return array result       [string bytes, int offset, bool overflow]
     */
    public function tailProcessLog () {
        return $this->request('supervisor.tailProcessLog', func_get_args());
    }

    /**
     * Provides a more efficient way to tail the (stderr) log than
     * readProcessStderrLog().  Use readProcessStderrLog() to read
     * chunks and tailProcessStderrLog() to tail.
     * Requests (length) bytes from the (name)'s log, starting at
     * (offset).  If the total log size is greater than (offset +
     * length), the overflow flag is set and the (offset) is
     * automatically increased to position the buffer at the end of
     * the log.  If less than (length) bytes are available, the
     * maximum number of available bytes will be returned.  (offset)
     * returned is always the last offset in the log +1.
     * @param string name         the name of the process (or 'group:name')
     * @param int offset          offset to start reading from
     * @param int length          maximum number of bytes to return
     * @return array result       [string bytes, int offset, bool overflow]
     */
    public function tailProcessStderrLog () {
        return $this->request('supervisor.tailProcessStderrLog', func_get_args());
    }

    /**
     * Provides a more efficient way to tail the (stdout) log than
     * readProcessStdoutLog().  Use readProcessStdoutLog() to read
     * chunks and tailProcessStdoutLog() to tail.
     * Requests (length) bytes from the (name)'s log, starting at
     * (offset).  If the total log size is greater than (offset +
     * length), the overflow flag is set and the (offset) is
     * automatically increased to position the buffer at the end of
     * the log.  If less than (length) bytes are available, the
     * maximum number of available bytes will be returned.  (offset)
     * returned is always the last offset in the log +1.
     * @param string name         the name of the process (or 'group:name')
     * @param int offset          offset to start reading from
     * @param int length          maximum number of bytes to return
     * @return array result       [string bytes, int offset, bool overflow]
     */
    public function tailProcessStdoutLog () {
        return $this->request('supervisor.tailProcessStdoutLog', func_get_args());
    }

    /**
     * Return an array listing the available method names
     * @return array result  An array of method names available (strings).
     */
    public function listMethods () {
        return $this->request('system.listMethods', func_get_args());
    }

    /**
     * Return a string showing the method's documentation
     * @param string name   The name of the method.
     * @return string result The documentation for the method name.
     */
    public function methodHelp () {
        return $this->request('system.methodHelp', func_get_args());
    }

    /**
     * Return an array describing the method signature in the
     * form [rtype, ptype, ptype...] where rtype is the return data type
     * of the method, and ptypes are the parameter data types that the
     * method accepts in method argument order.
     * @param string name  The name of the method.
     * @return array result  The result.
     */
    public function methodSignature () {
        return $this->request('system.methodSignature', func_get_args());
    }

    /**
     * Process an array of calls, and return an array of
     * results. Calls should be structs of the form {'methodName':
     * string, 'params': array}. Each result will either be a
     * single-item array containg the result value, or a struct of
     * the form {'faultCode': int, 'faultString': string}. This is
     * useful when you need to make lots of small calls without lots
     * of round trips.
     * @param array calls  An array of call requests
     * @return array result  An array of results
     */
    public function multicall () {
        return $this->request('system.multicall', func_get_args());
    }

}

$server = new SupervisorClient('http://localhost:8080/RPC2');
echo $server->test();